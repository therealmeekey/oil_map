from rest_framework import serializers
from .models import Pipeline, PipelinePoint


class PipelinePointSerializer(serializers.ModelSerializer):
    address = serializers.SerializerMethodField()

    class Meta:
        model = PipelinePoint
        fields = ['address']

    def get_address(self, obj):
        return [float(i) for i in obj.address.split(',')]


class PipelineSerializer(serializers.ModelSerializer):
    point = PipelinePointSerializer(many=True)
    start_point = serializers.SerializerMethodField()
    end_point = serializers.SerializerMethodField()

    class Meta:
        model = Pipeline
        fields = ['title', 'percent', 'start_point', 'end_point', 'point']

    def get_start_point(self, obj):
        return [float(i) for i in obj.start_point.address.split(',')] if obj.start_point else 'None'

    def get_end_point(self, obj):
        return [float(i) for i in obj.end_point.address.split(',')] if obj.end_point else 'None'
