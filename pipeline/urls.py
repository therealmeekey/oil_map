from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import PipeLineListView, PipeLinePointListView


urlpatterns = [
    url(r'^getLine/$', PipeLineListView.as_view()),
    url(r'^getPoint/$', PipeLinePointListView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
