import json
from .models import Pipeline
from django.db.models import signals
from django.dispatch import receiver
from django.db.models.signals import post_save
from channels import Group


@receiver(post_save, sender=Pipeline)
def send_update(sender, instance, **kwargs):
    Group("live").send({
        "text": json.dumps({
            "titile": instance.title,
            'percent': instance.percent,
            "start_point": [float(i) for i in instance.start_point.address.split(',')],
            'end_point': [float(i) for i in instance.end_point.address.split(',')]
        })
    })


def ws_add(message):
    message.reply_channel.send({"accept": True})
    Group("live").add(message.reply_channel)


def ws_disconnect(message):
    Group("live").discard(message.reply_channel)
