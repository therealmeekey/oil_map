from django.db import models
from django_ymap.fields import YmapCoord
from django.core.validators import MinValueValidator, MaxValueValidator
from factory.models import Factory


class Pipeline(models.Model):
    title = models.CharField(max_length=200, verbose_name='Название')
    percent = models.PositiveIntegerField(null=True, verbose_name='Процент наполнености',
                                          validators=[MinValueValidator(0), MaxValueValidator(100)])
    start_point = models.ForeignKey(Factory, on_delete=models.CASCADE, to_field="address",
                                    related_name='start_point', null=True, verbose_name='Начальная точка')
    end_point = models.ForeignKey(Factory, on_delete=models.CASCADE, to_field="address",
                                  related_name='end_point', null=True, verbose_name='Конечная точка')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'трубопровод'
        verbose_name_plural = 'Трубопроводы'


class PipelinePoint(models.Model):
    pipeline = models.ForeignKey('Pipeline', null=True, verbose_name='Промежуточная точка', on_delete=models.CASCADE,
                                 related_name='point')
    address = YmapCoord(max_length=200, start_query=u'Россия', size_width=500, size_height=500,
                        verbose_name='Промежуточная точка')

    class Meta:
        verbose_name = 'пункт'
        verbose_name_plural = 'Промежуточные точки'
