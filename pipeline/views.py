from rest_framework import generics
from .models import Pipeline, PipelinePoint
from .serializers import PipelineSerializer, PipelinePointSerializer


class PipeLineListView(generics.ListAPIView):
    queryset = Pipeline.objects.all()
    serializer_class = PipelineSerializer


class PipeLinePointListView(generics.ListAPIView):
    queryset = PipelinePoint.objects.all()
    serializer_class = PipelinePointSerializer
