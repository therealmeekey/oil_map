from rest_framework import generics
from .models import Scenario
from .serializers import ScenarioSerializer


class ScenarioListView(generics.ListAPIView):
    queryset = Scenario.objects.all()
    serializer_class = ScenarioSerializer
