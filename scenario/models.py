from django.db import models
from factory.models import Factory
from pipeline.models import Pipeline


class Scenario(models.Model):
    title = models.CharField(max_length=200, verbose_name='Название')

    def __str__(self):
        return self.title
        
    class Meta:
        verbose_name = 'Сценарий'
        verbose_name_plural = 'Сценарии'


class ScenarioExist(models.Model):
    scenario = models.ForeignKey('Scenario', related_name='scenario_exist', on_delete=models.CASCADE)
    factory = models.ForeignKey(Factory, related_name='factory', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'существующий объект'
        verbose_name_plural = 'Существующие объекты'


class PipelineTwo(models.Model):
    scenario = models.ForeignKey('Scenario', related_name='scenario_pipeline', on_delete=models.CASCADE)
    pipeline = models.ForeignKey(Pipeline, null=True, verbose_name='Промежуточная точка', on_delete=models.CASCADE,
                                 related_name='point_two')

    class Meta:
        verbose_name = 'трубопровод'
        verbose_name_plural = 'Трубопроводы'
