from rest_framework import serializers
from .models import Scenario, ScenarioExist, PipelineTwo
from pipeline.serializers import PipelineSerializer
from factory.serializers import FactorySerializer


class PipelineTwoSerializer(serializers.ModelSerializer):
    pipeline = PipelineSerializer()

    class Meta:
        model = PipelineTwo
        fields = ['pipeline']


class ScenarioExistSerializer(serializers.ModelSerializer):
    factory = FactorySerializer()

    class Meta:
        model = ScenarioExist
        fields = ['factory']


class ScenarioSerializer(serializers.ModelSerializer):
    scenario_pipeline = PipelineTwoSerializer(many=True)
    scenario_exist = ScenarioExistSerializer(many=True)

    class Meta:
        model = Scenario
        fields = ['title', 'scenario_pipeline', 'scenario_exist']
