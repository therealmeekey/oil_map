from django.contrib import admin
from django.urls import path, include
from maps import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from maps import views

urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls, name='admin'),
    path('', include('factory.urls')),
    path('', include('pipeline.urls')),
    path('', include('scenario.urls')),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)
