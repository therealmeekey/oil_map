from django.contrib import admin
from .models import Factory
from scenario.models import Scenario, ScenarioExist, PipelineTwo
from pipeline.models import Pipeline, PipelinePoint
from django_ymap.admin import YmapAdmin
from django.contrib.auth.models import User, Group


class PipelineTwoInline(YmapAdmin, admin.TabularInline):
    model = PipelineTwo
    insert_after = 'title'
    extra = 1


class ScenarioExistInline(YmapAdmin, admin.TabularInline):
    model = ScenarioExist
    insert_after = 'title'
    extra = 1


class PipelineInline(YmapAdmin, admin.TabularInline):
    model = PipelinePoint
    insert_after = 'address'
    extra = 1


class FactoryAdmin(YmapAdmin, admin.ModelAdmin):
    list_display = ('title', 'choice')
    list_filter = ('choice',)
    search_fields = ('title', 'choice')


class PipelineAdmin(YmapAdmin, admin.ModelAdmin):
    list_display = ('title', 'percent', 'start_point', 'end_point')
    search_fields = ('title',)
    inlines = [PipelineInline]


class ScenarioAdmin(YmapAdmin, admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    inlines = [ScenarioExistInline, PipelineTwoInline]


admin.site.site_header = 'Сайт Администратора'
admin.site.site_title = 'Сайт Администратора'
admin.site.site_url = '/index/'

admin.site.register(Factory, FactoryAdmin)
admin.site.register(Pipeline, PipelineAdmin)
admin.site.register(Scenario, ScenarioAdmin)
admin.site.unregister(User)
admin.site.unregister(Group)
