from django.urls import path
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *
from factory import views


urlpatterns = [
    path('index/', views.index),
    url(r'^getFactory/$', FactoryListView.as_view()),
    url(r'^getSump/$', SumpListView.as_view()),
    url(r'^getStorage/$', StorageListView.as_view()),
    url(r'^getPetrol/$', PetrolListView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
