from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Factory
from .serializers import FactorySerializer


class FactoryListView(APIView):

    def get(self, request, format=None):
        queryset = Factory.objects.filter(choice__in=['Factory'])
        serializer = FactorySerializer(queryset, many=True)
        return Response(serializer.data)


class SumpListView(APIView):

    def get(self, request, format=None):
        queryset = Factory.objects.filter(choice__in=['Sump'])
        serializer = FactorySerializer(queryset, many=True)
        return Response(serializer.data)


class StorageListView(APIView):

    def get(self, request, format=None):
        queryset = Factory.objects.filter(choice__in=['Storage'])
        serializer = FactorySerializer(queryset, many=True)
        return Response(serializer.data)


class PetrolListView(APIView):

    def get(self, request, format=None):
        queryset = Factory.objects.filter(choice__in=['Petrol'])
        serializer = FactorySerializer(queryset, many=True)
        return Response(serializer.data)


def index(request):
    return render(request, 'index.html')
