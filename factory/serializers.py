from rest_framework import serializers
from .models import Factory


class FactorySerializer(serializers.ModelSerializer):
    address = serializers.SerializerMethodField()

    class Meta:
        model = Factory
        fields = ['id', 'title', 'choice', 'address']

    def get_address(self, obj):
        return [float(i) for i in obj.address.split(',')]
