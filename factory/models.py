from django.db import models
from django_ymap.fields import YmapCoord


class Factory(models.Model):

    OBJECT_CHOICES = (
            ('Factory', 'Завод'),
            ('Sump', 'Вышка'),
            ('Storage', 'Хранилище'),
            ('Petrol', 'АЗС')
        )

    title = models.CharField(max_length=200, verbose_name='Название')
    choice = models.CharField(max_length=15, choices=OBJECT_CHOICES, default='', verbose_name='Объект')
    address = YmapCoord(max_length=200, start_query=u'Россия', size_width=500, size_height=500,
                        verbose_name='Метка на карте(Адрес)', unique=True)
    
    def __str__(self):
        return self.title
        
    class Meta:
        verbose_name = 'Объект'
        verbose_name_plural = 'Объекты'
